extends Node2D

const spanish_data = "res://data.bin"

var words = []
var index = -1
var cur_word = "supercalifragilisticoespiralidoso"
var rng = RandomNumberGenerator.new()
func _input(event):
	if event is InputEventKey and event.pressed:
		if event.key_label == KEY_ENTER:
			if get_node("table_top/LineEdit").text == cur_word:
				get_node("table_top/LineEdit").text = ""				
				initialize_grid()
			else:	
				print("La palabra no es correcta")
			
func _on_ready():
	get_words()
	initialize_grid()
	
func initialize_grid():
	index += 1
	get_node("table_top/LineEdit").grab_focus()
	var grid = get_node("table_top/GridContainer")
	for n in grid.get_children():
		grid.remove_child(n)
		n.queue_free() 
	if index >= len(words):
		Game.winner = true
		get_node("win").visible = true
		get_node("table_top/LineEdit").editable = false
		return
	cur_word = words[index]
	
	grid.columns = len(cur_word)
	var included = []
	for i in range(len(cur_word)):
		var tmp_node = Label.new()
		var rnd_number = int(rng.randf_range(0, len(cur_word)))
		while rnd_number in included:
			rnd_number = int(rng.randf_range(0, len(cur_word)))
		tmp_node.text = cur_word[rnd_number]
		included.append(rnd_number)
		grid.add_child(tmp_node)
		
func get_words():
	if FileAccess.file_exists(spanish_data):
		var file = FileAccess.open(spanish_data,FileAccess.READ)
		while not file.eof_reached():
			var tmpWord = file.get_line()
			if len(tmpWord) > 0:
				words.append(tmpWord)
	
	


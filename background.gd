extends ParallaxBackground
var scrolling_speed = 500

func _process(delta):
	if !Game.winner:
		scroll_offset.x -= scrolling_speed * delta
